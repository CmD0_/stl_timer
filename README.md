A small project to allow the learning of development in python with requests to the database of the Société des transports de Laval. 
The goal is to provide the times of the next bus passages of the STL according to the time and date of the day.

This project requires flask and requests as well as a working install of Python3!