#first(and only xD) code file for this project(we're not sure how it's going to end up looking(it ended up looking pretty nice xD))
from flask import Flask, request, send_from_directory, render_template
import requests
import xml.etree.ElementTree as ET

app = Flask(__name__,
            instance_relative_config=True,
            template_folder='src')

def getRouteData():
    r= requests.get('http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=stl')
    return ET.fromstring(r.text)

def getStopListData(tag):
    r= requests.get('http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=stl&r={0}'.format(tag))
    return ET.fromstring(r.text)

def getStopTimeData(id, tag):
    r =  requests.get('http://webservices.nextbus.com/service/publicXMLFeed?command=predictions&a=stl&stopId={0}&routeTag={1}'.format(id, tag))
    return ET.fromstring(r.text)

Bus = ""

Stop = ""
@app.route("/times_list", methods = ['POST'])
def test_final():
    Bus = request.form['Bus']
    Stop = str(request.form['Stop']).replace("CP", "")
    root = getStopTimeData(Stop, Bus)
    html = '''<!DOCTYPE html>
<html lang="en">
  <head>
    <title>TimerDeLaStl</title>
    <meta charset="utf-8">

  </head>
  <body>
    <h1 align="center" >Liste des temps de passage</h1>
    <div class="container">
    {table}
    </div>
  </body>
</html>
'''
    table = '<ul align="center" >'
    try:
        for p in  root.iter('prediction'):
            table += '<li><tr><td>{0} Minutes</td></ul>'.format(p.attrib['minutes'])
    except:
        pass
    table += '</ul>'

    html = html.format(table=table)
    return html

@app.route("/stop_setting", methods=['POST'])
def text_receiver():
    Bus = request.form['Bus Line']
    root = getStopListData(Bus)
    html = '''<!DOCTYPE html>
<html lang="en">
   <head>
        <title>TimerDeLaStl</title>
        <meta charset="utf-8">

    </head>
    <body>
    <h1 align="center" >Parametres</h1>
    <div class="form" align="center">
        <form action="/times_list" method="post">
            Bus Line:<br>
            <select name="Bus">
                <option value="{Bus}">Your Bus Line(select this)</option>
            </select>
            Stop:<br>
            <select name="Stop">
                {table}
            </select>
            <input type="submit" value="Submit">
      </form>
   </div>
   </body>
</html>
'''
    table =""
    try:
        for data in root.iter('stop'):
            table += '<option value="{StopID}">{StopID} {title}</option>'.format(StopID = data.attrib['tag'], title = data.attrib['title'])
    except:
        pass
    
    html = html.format(table = table, Bus = Bus)
    return html
@app.route("/")
def test():
    root =  getRouteData()
    html = '''<!DOCTYPE html>
<html lang="en">
   <head>
        <title>TimerDeLaStl: Bus Line</title>
        <meta charset="utf-8">

    </head>
    <body>
    <h1 align="center" >TimerDeLaStl</h1>
    <div class="form" align="center">
        <form action="/stop_setting" method="post">
            Bus Line:<br>
            <select name="Bus Line">
                {table}
            </select>
            <input type="submit" value="Submit">
      </form>
   </div>
   </body>
</html>
'''

    table = ""
    for data in root:
        table +='<option value="{RouteTag}">{RouteTag} {title}</option>'.format(RouteTag = data.attrib['tag'], title = data.attrib['title'])
    
    html = html.format(table=table)
    return html

if __name__=="__main__":
    app.run()
